import argparse
import subprocess

# configuration

MAC_ADDR = "D0:B5:C2:AF:47:18"
HANDLE = "0x002e"
POWER_ON = "cc2333"
POWER_OFF = "cc2433"

#color signal 56RRGGBB00f0aa where RRGGBB is a hex color without '0x' prefix
COLOR_START = "56"
COLOR_END = "00f0aa"


def is_hex(s):
    try:
        int(s, 16)
        return True
    except ValueError:
        return False


def is_valid(s):
    if (s == "on" or s == "off" or is_hex(s)):
        return True
    else:
        return False


parser = argparse.ArgumentParser(description="Control MagicLight Bluetooth LED Smart Bulb")
parser.add_argument("value", help="on, off, RRGGBB color")

args = parser.parse_args()

cmd = "gatttool -b " + MAC_ADDR + " --char-write -a " + HANDLE + " -n "

if args.value == "on":
    cmd += POWER_ON
    subprocess.Popen(cmd,shell=True)
    print(args.value)
elif args.value == "off":
    cmd += POWER_OFF
    subprocess.Popen(cmd,shell=True)
    print(args.value)
elif is_hex(args.value):
    cmd+= (COLOR_START + args.value + COLOR_END)
    subprocess.Popen(cmd,shell=True)
    print(args.value)
else:
    print("Invalid value entered, please try again")
