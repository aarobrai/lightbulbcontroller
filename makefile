#Thanks Bob
#https://cs.wmich.edu/~bhardin/reference/makefile.txt 

CC = gcc
# Unset/Set CFLAGS for sanity checks and debugging 
# CFLAGS = -pedantic -Wall -g -DDEBUG
#CFLAGS = -pedantic -Wall

light:
	${CC} ${CFLAGS} -o light light.c
clean:
	rm -f *.o light