#include <stdio.h>
#include <stdlib.h>

#define BULB_MAC_ADDR "D0:B5:C2:AF:47:18"
#define HANDLE "0x002e"

int main(int argc, char* argv[])
{

	if(argc == 0) {
		printf("Usage: light [FFFFFF]\n");
		exit(0);
	}

	char command[128];
    snprintf(command, sizeof(command), "sudo gatttool -b %s --char-write -a %s -n 56%s00f0aa",
         BULB_MAC_ADDR,HANDLE,argv[1]);
         
    printf("%s\n",command);

	system(command);

	exit(0);

}